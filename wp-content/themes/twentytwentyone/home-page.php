<?php
/**
 * The template for displaying all single posts
 * Template Name: Home page
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();?>
    <main>
        
        <section class="miniMundo_apresentation showSection">
            
            <div class="background-carousel">
                <?php 
                    $params = array(     
                    'limit' => -1,   
                    'orderby' => 'post_date DESC',
                ); 
                $banner = pods( 'banner_para_home', $params ); 
                while ( $banner->fetch() ) {    ?>
                    <div class="background_image">
                        <?php 

                            if (!empty($banner->display('banner_desktop')) && !empty($banner->display('banner_mobile'))) { ?>
                                <img class="desktop_image" src="<?php echo $banner->display( 'banner_desktop'); ?>" alt="<?php echo $banner->display( 'title' ); ?>">
                                <img class="mobile_image" src="<?php echo $banner->display( 'banner_mobile'); ?>" alt="<?php echo $banner->display( 'title' ); ?>">
                            <?php  } elseif (!empty($banner->display('url_do_video')) && !empty($banner->display('url_do_gif'))) { ?>
                                <video autoplay muted loop>
                                    <source src="<?php echo $banner->display( 'url_do_video'); ?>" type="video/mp4">
                                </video>
                                <img src="<?php echo $banner->display( 'url_do_gif'); ?>" alt="<?php echo $banner->display( 'title' ); ?>">
                            <?php } else { ?>
                                <img class="desktop_image" src="https://minimundo.com.br/wp-content/uploads/2023/06/placeholder_desktop.png" alt="<?php echo $banner->display( 'title' ); ?>">
                                <img class="mobile_image" src="https://minimundo.com.br/wp-content/uploads/2023/06/placeholder_mobile.png" alt="<?php echo $banner->display( 'title' ); ?>">
                          <?php   }
                        ?>
                    </div>
                <?php } ?>       
            </div>

            <div class="miniMundo_information">
                <h1><?php echo get_post_meta(get_the_ID(), 'Título', true); ?></h1>
                <p><?php echo get_post_meta(get_the_ID(), 'Sub-título', true); ?></p>
            </div>
            
            <a class="anchor" href="#knowmore">saiba mais</a>
            <a style="background-image: none!important;" href="https://minimundo.com.br/parque?utm_source=banner&utm_medium=banner&utm_campaign=br-site-lp-banner-banner-atelogo_bannerhome">saiba mais</a>
            <span id="knowmore"></span>
        </section>

        <section class="containerGallery">

            <div class="carousel_Gallery">
                <?php 
                    $melhores_angulos = pods( 'melhor_angulo' );
                    $params = array(     
                        'limit' => -1,   
                        'orderby' => 'post_date DESC',
                    ); 
                    $melhores_angulos = pods( 'melhor_angulo', $params ); 
                    while ( $melhores_angulos->fetch() ) {    ?>
                        <div class="gallery_card">
                            <div class="card_image">
                                <img src="<?php echo $melhores_angulos->display( 'imagem' );?>" alt="<?php echo $melhores_angulos->display( 'title' );?>">
                            </div>
                            <div class="description">
                                <p><?php echo $melhores_angulos->display( 'title' );?></p>
                            </div>
                        </div>  
                <?php } ?>
            </div>

        </section>
        
        <section class="containerInformationNewspaper">
            <div class="information_downloadNewspaper">
                <div class="text">
                    <h2>Baixe gratuitamente a última edição do Jornal Mini Mundo.</h2>
                    <p>Com ela você tem acesso aos acontecimentos de nossa cidade em miniatura, o mapa local e muito mais.</p>
                    <?php 
                        $pods_baixar_jornal = pods( 'baixar_jornal' );
                        $params = array(     
                            'limit' => -1,   
                            'orderby' => 'post_date DESC',
                        ); 
                        $pods_baixar_jornal = pods( 'baixar_jornal', $params ); ?>
                        <a href="<?php echo $pods_baixar_jornal->display( 'download_jornal' );?>" download>
                            baixar jornal
                        </a>
                        <a class="download_mobile" href="<?php echo $pods_baixar_jornal->display( 'download_jornal' );?>" download>
                            baixar
                        </a>
                </div>
                <div class="imagemJornal">
                    <img src="<?php echo $pods_baixar_jornal->display( 'imagem' );?>" alt="title">
                </div>
            </div>
        </section>

        <!-- <section class="container_news">
            <h2>Últimas Notícias</h2>
    
            <div class="carousel-Newspaper">
                <?php 
                    $args = array(
                        'category_name' => 'blog',
                        'post_status' => 'publish',
                    ); 
                ?>
                <?php query_posts($args); ?>
                <?php
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post();?>

                            <a href="<?php echo get_permalink();?>">
                                <div class="card_image">
                                    <?php echo get_the_post_thumbnail()?>
                                </div>

                                <div class="newsInformation">
                                    <?php $tag = get_the_tags();
                                        if ( ! empty( $tag ) ) {
                                        echo '<span>' . esc_html( $tag[0]->name ) . '</span>';
                                    }?>
                                    <p><?php the_title();?></p>
                                    <?php echo get_the_date('d/m/y'); ?>    
                                </div>
                            </a>
						<?php };
					}
				?>			

              
            </div>
    
            <a class="moreInformation" href="<?php echo home_url();?>/blog">CONFIRA MAIS NOTÍCIAS</a>
        </section> -->

        <section class="container-spaceInformation">
            <h2>Mini Mundo em números</h2>
            <div class="containerArea_information">
                <?php 
                    $pods_mini_em_numero = pods( 'mini_em_numero' );
                    $params = array(     
                        'limit' => -1,   
                        'orderby' => 'post_date DESC',
                    ); 
                    $pods_mini_em_numero = pods( 'mini_em_numero', $params ); 
                    while ( $pods_mini_em_numero->fetch() ) {    ?>
                       <div>
                           <img src="<?php echo $pods_mini_em_numero->display( 'imagem' );?>" alt="<?php echo $pods_mini_em_numero->display( 'title' );?>">
                           <h2><?php echo $pods_mini_em_numero->display( 'title' );?></h2>
                           <?php echo $pods_mini_em_numero->display( 'content' );?>
                       </div>
                <?php } ?>
        
                <!-- <div>
                    <span></span>
                    <h2>90 minutos</h2>
                    tempo médio de visitação
                </div>
        
                <div>
                    <span></span>
                    <h2>3.268</h2>
                    Mini-habitantes no parque
                </div> -->
        
                <!-- <div>
                    <span></span>
                    <h2>290</h2>
                    animais
                </div>
        
                <div>
                    <span></span>
                    <h2>505</h2>
                    veículos
                </div>
        
                <div>
                    <span></span>
                    <h2>133</h2>
                    construções
                </div> -->
            </div>
        </section>
      
    </main>    

<?php get_footer();
