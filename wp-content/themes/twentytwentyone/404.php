<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();
	?>
		<main>
			<section class="not_found">
				<img src="<?php echo get_template_directory_uri(); ?>/images/ilustra-404.png" alt="ilustração 404">
				<h1>Ops... página não encontrada.</h1>
				<p>Não conseguimos encontrar o que você estava procurando, a página não existe ou foi removida. Clique no botão para voltar para  a página inicial.</p>
				<a href="<?php echo home_url();?>">VOLTAR À PÁGINA INICIAL</a>
			</section>
		</main>

	<?php
get_footer();
