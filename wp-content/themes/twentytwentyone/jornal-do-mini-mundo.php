<?php
/**
 * The template for displaying all single posts
 * Template Name: Jornal do mini mundo
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();?>
        <main>
            <section class="newspaper_edition">
                <h1 id="knowmore" >Jornal do Mini Mundo</h1>
                <p class="text_center">Confira as edições de jornal com as notícias da mini cidade.</p>

                <div class="newspaper_content">
                        <?php 
                            $pod_jornal = pods( 'jornal' );
                            $params = array(     
                                'limit' => -1,   
                                'orderby' => 'post_date DESC',
                            ); 
                            $pod_jornal = pods( 'jornal', $params ); 
                            while ( $pod_jornal->fetch() ) { ?>
                            <div class="editionCard">
                                <div class=information>
                                    <h2><?php echo $pod_jornal->display( 'title' );?></h2>
                                    <?php echo $pod_jornal->display( 'excerpt' );?>
                                    <div class="containerButtons">
                                        <a href="<?php echo $pod_jornal->display( 'permalink' );?>">LER</a>
                                        <a class="downloadButton" href="<?php echo $pod_jornal->display( 'baixar_jornal' );?>" download></a>
                                    </div>
                                </div>
                                <div class=news>
                                    <img src="<?php echo $pod_jornal->display( 'imagem' );?>" alt="<?php echo $pod_jornal->display( 'title' );?>">
                                </div>
                            </div>
                        <?php } ?>
                        
                </div>
                <div class="button_block">
                    <div>CARREGAR MAIS EDIÇÕES</div>
                    <div>CARREGAR MENOS EDIÇÕES</div>
                </div>
            </section>

            <!-- <section class="containerInformationNewspaper">
                <div class="information_downloadNewspaper">
                    <div class="text">
                        <h2>Baixe gratuitamente a edição do Jornal Mini Mundo</h2>
                        <p>Com a última edição você tem acesso a notícias de acontecimentos do mundo, o mapa local e muito mais!</p>
                        <?php 
                            $pods_baixar_jornal = pods( 'baixar_jornal' );
                            $params = array(     
                                'limit' => -1,   
                                'orderby' => 'post_date DESC',
                            ); 
                            $pods_baixar_jornal = pods( 'baixar_jornal', $params ); ?>
                            <a href="<?php echo $pods_baixar_jornal->display( 'download_jornal' );?>" download>
                                baixar jornal
                            </a>
                            <a class="download_mobile" href="<?php echo $pods_baixar_jornal->display( 'download_jornal' );?>" download>
                                baixar
                            </a>
                    </div>
                    <div class="imagemJornal">
                        <img src="<?php echo $pods_baixar_jornal->display( 'imagem' );?>" alt="title">
                    </div>
                </div>
            </section> -->
        </main>

<?php get_footer();
