<?php
/**
 * The template for displaying all single posts
 * Template Name: Contato
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();?>
    <main>
        <section class="localization">
            <h1 id="knowmore">Contato</h1>
            <p class="text_center" >Saiba como nos encontrar!</p>

            <div class="contact">
                <div class="containerContact">
                    <p class="tel">
                        <span></span> 
                        (54) 3286.4055
                    </p>

                    <ul class="social_networks">
                        <li class="facebook">
                            <a href="https://www.facebook.com/MiniMundoGramado" target="_blank"></a>
                        </li>
                        
                        <li class="instagram">
                            <a href="https://www.instagram.com/minimundogramado/" target="_blank"></a>
                        </li>
                    </ul>

                </div>

                <p class="email">
                    <span></span> 
                    parque@minimundo.com.br
                </p>

                <p>Trabalhe conosco: rh@minimundo.com.br</p>
            </div>

            <div class="map">
                <p>
                    <span></span>
                    Rua Horácio Cardoso, 291 - Gramado - RS, CEP 95670-000
                </p>

                <iframe 
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13906.054499232629!2d-50.8754059!3d-29.3845272!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x11b129a1a6182575!2sMini%20Mundo!5e0!3m2!1spt-BR!2sbr!4v1652793947920!5m2!1spt-BR!2sbr"
                    width="600"
                    height="450" 
                    style="border:0;" 
                    allowfullscreen="" 
                    loading="lazy" 
                    referrerpolicy="no-referrer-when-downgrade"
                    >
                </iframe>
            </div>
        </section>

        <section class="doubts">
            <h2>Ainda está com dúvidas?</h2>
            <p class="text_center">Preencha os campos abaixo com os seus dados e deixe uma mensagem para que possamos entrar em contato.</p>
            
            <?php
                echo do_shortcode( '[contact-form-7 id="5" title="Formulário de contato"]' );
            ?>
        </section>
    </main>

<?php get_footer();
