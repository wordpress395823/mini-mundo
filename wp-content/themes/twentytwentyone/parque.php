<?php
/**
 * The template for displaying all single posts
 * Template Name: O parque
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();?>
    <main>
        <section class="thePark miniMundo_apresentation showSection">
            <div class="background-carousel">
                <?php 
                    $params = array(     
                    'limit' => -1,   
                    'orderby' => 'post_date DESC',
                ); 
                $banner = pods( 'banner_do_parque', $params ); 
                while ( $banner->fetch() ) {    ?>
                    <div class="background_image">
                        <?php 

                            if (!empty($banner->display('banner_desktop')) && !empty($banner->display('banner_mobile'))) { ?>
                                <img class="desktop_image" src="<?php echo $banner->display( 'banner_desktop'); ?>" alt="<?php echo $banner->display( 'title' ); ?>">
                                <img class="mobile_image" src="<?php echo $banner->display( 'banner_mobile'); ?>" alt="<?php echo $banner->display( 'title' ); ?>">
                            <?php  } elseif (!empty($banner->display('url_do_video')) && !empty($banner->display('url_do_gif'))) { ?>
                                <video autoplay muted loop>
                                    <source src="<?php echo $banner->display( 'url_do_video'); ?>" type="video/mp4">
                                </video>
                                <img src="<?php echo $banner->display( 'url_do_gif'); ?>" alt="<?php echo $banner->display( 'title' ); ?>">
                            <?php } else { ?>
                                <img class="desktop_image" src="https://minimundo.com.br/wp-content/uploads/2023/06/placeholder_desktop.png" alt="<?php echo $banner->display( 'title' ); ?>">
                                <img class="mobile_image" src="https://minimundo.com.br/wp-content/uploads/2023/06/placeholder_mobile.png" alt="<?php echo $banner->display( 'title' ); ?>">
                          <?php   }
                        ?>
                    </div>
                <?php } ?>       
            </div>

            <div class="miniMundo_information">
                <h1><?php echo get_post_meta(get_the_ID(), 'Título', true); ?></h1>                
                <p><?php echo get_post_meta(get_the_ID(), 'Sub-título', true); ?></p>
            </div>
            <span id="knowmore"></span>

        </section>

        <section class="mobile_information">
            <div class="miniMundo_information">
                <h2 class="title"><?php echo get_post_meta(get_the_ID(), 'Título', true); ?></h2>
                <p><?php echo get_post_meta(get_the_ID(), 'Sub-título', true); ?></p>
            </div>
        </section>

        <section class="containerGallery galleryPark">

            <div class="carousel_Gallery">
                <?php 
                    $parque_galeria_pod = pods( 'parque_galeria' );
                    $params = array(     
                        'limit' => -1,   
                        'orderby' => 'post_date DESC',
                    ); 
                    $parque_galeria_pod = pods( 'parque_galeria', $params ); 
                    while ( $parque_galeria_pod->fetch() ) {    ?>
                        <div class="gallery_card">
                            <div class="card_image">
                                <img src="<?php echo $parque_galeria_pod->display( 'imagem' );?>" alt="<?php echo $parque_galeria_pod->display( 'title' );?>" class="lightboxed" rel="group1" >
                            </div>
                        </div>  
                <?php } ?>
            </div>

        </section>

        <section  class="attractions">
            <!--<h2>Nossas atrações</h2>
            <p class="text_center">Você já conhece as atrações do parque? Confira abaixo:</p>-->

            <div class="accordion_container">  
                <?php 
                    $acordeon_atracoes = pods( 'atracoes' );
                    $params = array(     
                        'limit' => -1,   
                        'orderby' => 'post_date ASC',
                    ); 
                    $acordeon_atracoes = pods( 'atracoes', $params ); 
                    while ( $acordeon_atracoes->fetch() ) {    ?>
                        <div class="accordion_card">
                            <h3><?php echo $acordeon_atracoes->display( 'title' );?></h3>
                            <div class="image_card">
                                <img src="<?php echo $acordeon_atracoes->display( 'imagem' );?>" alt="<?php echo $acordeon_atracoes->display( 'title' );?>"> 
                            </div>
                            <div class="description_card">
                                <div class="open_close">
                                    <span></span>
                                    <span></span>
                                </div>
                                    <p>
                                        <?php echo $acordeon_atracoes->display( 'content' ); ?>
                                    </p>
                                <!--
                                <div> 
                                    <h3>atendimento</h3>
                                    <div class="date">
                                        <span></span>
                                        <p class="more_operation">                  
                                            <?php echo $acordeon_atracoes->display( 'funcionamento' ); ?>
                                        </p>
                                    </div>
                                    <a href="<?php echo $acordeon_atracoes->display( 'url' ); ?>" target="_blank">
                                        <span></span>
                                        <p class="more_website">
                                            <?php echo $acordeon_atracoes->display( 'nome_da_url' ); ?>
                                        </p>
                                    </a>

                                    <p>
                                    <span class="phone"></span><?php echo $acordeon_atracoes->display( 'telefone' ); ?>
                                    </p>
                                </div>
                            -->
                            </div>
                        </div>
                <?php } ?>
            </div>
        </section>

        <section class="timeline">
            <h2>Nossa história</h2>
            <p class="text_center ">O Mini Mundo nasceu de um sonho, há mais de trinta anos. Motivados em criar um mundo melhor e mais feliz, um pai e um avô construíram uma Casinha de Bonecas e uma mini ferrovia para suas crianças, começando assim a história de nossa cidade em miniatura. Hoje, já na quarta geração, a família segue construindo e mantendo esta obra repleta de amor e cuidado. Confira os detalhes em nossa linha do tempo:</p>

            <div class="carousel_timeline_Gallery">                                    
                <?php 
                    $pod_nossa_historia = pods( 'nossa_historia' );
                    $params = array(     
                        'limit' => -1,   
                        'orderby' => 'post_date ASC',
                    ); 
                    $pod_nossa_historia = pods( 'nossa_historia', $params ); 
                    while ( $pod_nossa_historia->fetch() ) {    ?>
                    <div class="timeline_card">  
                        <div class="timeline_nav" >
                            <a href="#<?php echo $pod_nossa_historia->display( 'title' );?>"></a>
                            <p><?php echo $pod_nossa_historia->display( 'title' );?></p>
                        </div>   
                        <a data-hash="<?php echo $pod_nossa_historia->display( 'title' );?>" class="timeline_content">
                            <div class="image_card">
                                <img src="<?php echo $pod_nossa_historia->display( 'imagem' );?>" alt="<?php echo $pod_nossa_historia->display( 'titulo' );?>"> 
                            </div>
                            <div class="description">
                                <h3>
                                    <?php echo $pod_nossa_historia->display( 'titulo' );?>
                                </h3>
                                <?php echo $pod_nossa_historia->display( 'content' );?>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>  
        </section>

        <section class="our_mascots">
            <h2>Nossos personagens</h2>
            <p class="text_center">Separamos algumas curiosidades sobre os nossos personagens para que você conheça um pouco mais de cada um deles.</p>

            <div class="carousel_mascots">
                
                <?php 
                    $pod_nosso_mascote = pods( 'nosso_mascote' );
                    $params = array(     
                        'limit' => -1,   
                        'orderby' => 'post_date DESC',
                    ); 
                    $pod_nosso_mascote = pods( 'nosso_mascote', $params ); 
                    while ( $pod_nosso_mascote->fetch() ) {    ?>
                        <div class="mascots_card">
                            <div class="card_image">
                                <img src="<?php echo $pod_nosso_mascote->display( 'imagem' );?>" alt="<?php echo $pod_nosso_mascote->display( 'title' );?>"> 
                            </div>
                            <div class="mascots_information">
                                <h3><?php echo $pod_nosso_mascote->display( 'title' );?></h3>
                                <p>
                                    <?php echo $pod_nosso_mascote->display( 'content' );?>
                                </p>
                            </div>
                        </div>
                <?php } ?>
            </div>
        </section>
    </main>    

<?php get_footer();
