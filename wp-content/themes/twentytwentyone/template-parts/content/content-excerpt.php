<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
	<a href="<?php echo get_permalink();?>">
		<div class="card_image">
			<?php echo get_the_post_thumbnail()?>	
		</div>
		<div class="newsInformation">
			<?php $tag = get_the_tags();
				if ( ! empty( $tag ) ) {
					echo '<span class="tag">' . esc_html( $tag[0]->name ) . '</span>';
				}
			?>
			<p><?php the_title();?></p>
			<span class="date">
				<?php echo get_the_date('d/m/y'); ?>
			</span>
		</div>
	</a>
	

